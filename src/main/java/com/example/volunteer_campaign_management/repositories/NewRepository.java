package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.NewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewRepository extends JpaRepository<NewEntity,Integer> {
}
