package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<AccountEntity,Integer> {

    List<AccountEntity> getAllAccounts();
    AccountEntity findByEmail(String email);
    AccountEntity findByEmailAndPassword(String email, String password);
}
