package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.TaskReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskReportRepository extends JpaRepository<TaskReportEntity,Integer> {
}
