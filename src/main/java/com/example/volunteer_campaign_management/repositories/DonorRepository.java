package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.DonorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DonorRepository extends JpaRepository<DonorEntity,Integer> {
}
