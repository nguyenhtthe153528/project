package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<ProfileEntity,Integer> {
}
