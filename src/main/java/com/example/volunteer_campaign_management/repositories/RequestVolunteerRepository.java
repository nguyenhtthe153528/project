package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.RequestVolunteerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestVolunteerRepository extends JpaRepository<RequestVolunteerEntity,Integer> {
}
