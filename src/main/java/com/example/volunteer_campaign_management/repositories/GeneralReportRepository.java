package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.GeneralReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GeneralReportRepository extends JpaRepository<GeneralReportEntity,Integer> {
}
