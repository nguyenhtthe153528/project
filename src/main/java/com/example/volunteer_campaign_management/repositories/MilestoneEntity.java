package com.example.volunteer_campaign_management.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MilestoneEntity extends JpaRepository<MilestoneEntity,Integer> {
}
