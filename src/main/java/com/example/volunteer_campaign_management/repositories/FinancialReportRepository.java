package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.FinancialReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FinancialReportRepository extends JpaRepository<FinancialReportEntity,Integer> {
}
