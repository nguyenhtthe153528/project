package com.example.volunteer_campaign_management.repositories;


import com.example.volunteer_campaign_management.entities.DepartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<DepartmentEntity,Integer> {

}
