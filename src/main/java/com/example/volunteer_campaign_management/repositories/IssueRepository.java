package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.IssueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IssueRepository extends JpaRepository<IssueEntity,Integer> {
}
