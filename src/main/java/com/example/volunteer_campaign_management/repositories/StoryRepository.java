package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.StoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoryRepository extends JpaRepository<StoryEntity,Integer> {
}
