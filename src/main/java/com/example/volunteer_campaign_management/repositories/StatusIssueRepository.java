package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.StatusIssueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusIssueRepository extends JpaRepository<StatusIssueEntity,Integer> {
}
