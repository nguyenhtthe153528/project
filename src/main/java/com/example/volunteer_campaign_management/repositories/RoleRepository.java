package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity,Integer> {
    RoleEntity findByName(String name);
}
