package com.example.volunteer_campaign_management.repositories;

import com.example.volunteer_campaign_management.entities.MediaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaRepository extends JpaRepository<MediaEntity,Integer> {
}
