package com.example.volunteer_campaign_management.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class IssueEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int issueId;
    private String title;
    private String description;
    private boolean prioriry;
    private String assignee;
    private Timestamp due_date;

}
