package com.example.volunteer_campaign_management.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class NewEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int newId;
    private String title;
    private String content;
    private Timestamp created_at;

}
