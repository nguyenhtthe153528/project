package com.example.volunteer_campaign_management.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class RequestVolunteerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int requestVolunteerId;
    private String name;
    private String email;
    private String phone;
    private Timestamp date_of_birth;
    private String address;
    private String department_request;
    private String time_free;
    private boolean status;

}
