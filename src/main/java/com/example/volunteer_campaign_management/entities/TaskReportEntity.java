package com.example.volunteer_campaign_management.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskReportEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int taskReport_Id;
    private String name;
    private String title;
    private Timestamp due_date;
    private String note;

}
