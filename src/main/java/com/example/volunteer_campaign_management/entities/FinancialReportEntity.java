package com.example.volunteer_campaign_management.entities;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class FinancialReportEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int financialReportId;
    private String name;
    private String amount;
    private Double total_expense;
    private String assign_to;
    private String description;
    private Timestamp created_at;

}
