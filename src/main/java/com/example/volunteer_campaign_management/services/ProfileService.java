package com.example.volunteer_campaign_management.services;

import com.example.volunteer_campaign_management.entities.ProfileEntity;

public interface ProfileService {
    ProfileEntity profileById(int accountId);
}
