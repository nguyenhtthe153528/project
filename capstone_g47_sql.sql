create database capstone_group47;
use capstone_group47;

create table IF NOT EXISTS User(
user_id int primary key not null,
password varchar(255) not null,
email varchar(255) not null,
phone varchar(20) not null,
role_id int not null,
profile_id int not null,
department_id int not null,
FOREIGN KEY (role_id) REFERENCES Role(role_id),
FOREIGN KEY (profile_id) REFERENCES Profile(profile_id),
FOREIGN KEY (department_id) REFERENCES Department(department_id)
);

create table IF NOT EXISTS Role(
role_id int primary key not null,
name nvarchar(255) not null,
description nvarchar(255) not null
);

create table IF NOT EXISTS Profile(
profile_id int primary key not null,
first_name nvarchar(255) not null,
last_name nvarchar(255) not null,
avatar text not null,
address nvarchar(255) not null,
status boolean not null
);

create table IF NOT EXISTS Department(
department_id int primary key not null,
name nvarchar(255) not null,
description nvarchar(255) not null
);

create table IF NOT EXISTS Campaign(
campaign_id int primary key not null,
name nvarchar(255) not null,
start_date date not null,
end_date date not null,
description nvarchar(255) not null,
title nvarchar(255) not null,
location nvarchar(255) not null,
status_issue_id int not null,
FOREIGN KEY (status_issue_id) REFERENCES Status_Issue(status_issue_id)
);

create table IF NOT EXISTS TaskReport(
taskreport_id int primary key not null,
name nvarchar(255) not null,
description nvarchar(255) not null,
title nvarchar(255) not null,
due_date date not null,
note text not null,
status_id int not null,
campaign_id int not null,
issue_id int not null,
FOREIGN KEY (campaign_id) REFERENCES Campaign(campaign_id),
FOREIGN KEY (issue_id) REFERENCES Issues(issue_id)
);

create table IF NOT EXISTS Story(
story_id int primary key not null,
name nvarchar(255) not null,
content nvarchar(255) not null,
title nvarchar(255) not null,
created_at date not null,
user_id int not null,
campaign_id int not null,
id int not null,
FOREIGN KEY (id) REFERENCES Media(id),
FOREIGN KEY (campaign_id) REFERENCES Campaign(campaign_id)
);

create table IF NOT EXISTS FinancialReport(
financialreport_id int primary key not null,
name nvarchar(255) not null,
amount decimal(19,4) not null,
total_expenses decimal(19,4) not null,
assign_to text not null,
description nvarchar(255) not null,
created_at date not null,
note text not null,
user_id int not null,
campaign_id int not null,
FOREIGN KEY (campaign_id) REFERENCES Campaign(campaign_id)
);

create table IF NOT EXISTS Donors(
donor_id int key not null,
name nvarchar(255) not null,
image text not null,
amount decimal(19,4) not null,
donate_date date not null,
description nvarchar(255) not null,
campaign_id int not null,
user_id int not null,
FOREIGN KEY (campaign_id) REFERENCES Campaign(campaign_id)
);

create table IF NOT EXISTS Status_Issue(
status_issue_id int primary key not null,
name nvarchar(255) not null,
description nvarchar(255) not null
);

create table IF NOT EXISTS Milestiones(
milestione_id int primary key not null,
name nvarchar(255) not null,
description nvarchar(255) not null,
create_at date not null,
end_date date not null,
campaign_id int not null,
user_id int not null,
status_issue_id int not null,
FOREIGN KEY (campaign_id) REFERENCES Campaign(campaign_id),
FOREIGN KEY (status_issue_id) REFERENCES Status_Issue(status_issue_id)
);

create table IF NOT EXISTS Media(
id int primary key not null,
campaign_id int not null,
image text not null,
video varchar(255) not null,
FOREIGN KEY (campaign_id) REFERENCES Campaign(campaign_id)
);

create table IF NOT EXISTS RequestVolunteer(
id int primary key not null,
name nvarchar(255) not null,
email varchar(255) not null,
phone varchar(20) not null,
date_of_birth date not null,
address nvarchar(255) not null,
department_request nvarchar(255) not null,
time_free nvarchar(255) not null,
user_id int not null,
status boolean not null,
campaign_id int not null,
FOREIGN KEY (campaign_id) REFERENCES Campaign(campaign_id)
);

create table IF NOT EXISTS News(
new_id int primary key not null,
title nvarchar(255) not null,
content nvarchar(255) not null,
created_date date not null,
user_id int not null,
id int not null,
FOREIGN KEY (user_id) REFERENCES User(user_id),
FOREIGN KEY (id) REFERENCES Media(id)
);
 
create table IF NOT EXISTS Issues(
issue_id int primary key not null,
title nvarchar(255) not null,
description nvarchar(255) not null,
priority boolean not null,
assignee text not null,
due_date date not null,
status_issue_id int not null,
user_id int not null,
FOREIGN KEY (user_id) REFERENCES User(user_id),
FOREIGN KEY (status_issue_id) REFERENCES Status_Issue(status_issue_id)
);

create table IF NOT EXISTS GeneralReport(
generalreport_id int not null,
attachment varchar(255) not null,
created_at date not null,
user_id int not null,
campaign_id int not null,
status_issue_id int not null,
FOREIGN KEY (status_issue_id) REFERENCES Status_Issue(status_issue_id),
FOREIGN KEY (campaign_id) REFERENCES Campaign(campaign_id)
)
